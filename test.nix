{ pkgs ? import <nixpkgs> {}
, callPackage ? pkgs.callPackage
, circularFixpoint ? callPackage ./circular-fixpoint.nix {}
}:

callPackage ({lib}:
  let
    inherit (circularFixpoint) mergeCycles;
    inherit (lib) concatStringsSep;
    inherit (lib.lists) flatten;
    merge = (xs: { name = concatStringsSep "+" (map (node: node.name) xs);
                   deps = flatten (map (node: node.deps) xs); });
  in
  {
    noCycles = mergeCycles merge
      { a = { name = "a"; deps = [ "b" ]; };
        b = { name = "b"; deps = []; }; }
      "deps" "a";
    smallestCycle = mergeCycles merge
      { a = { name = "a"; deps = [ "b" ]; };
        b = { name = "b"; deps = [ "a" ]; }; }
      "deps" "a";
    smallCycleNonCycleDeps = mergeCycles merge
      { a = { name = "a"; deps = [ "b" "c" ]; };
        b = { name = "b"; deps = [ "a" "d" ]; };
        c = { name = "c"; deps = []; };
        d = { name = "d"; deps = []; }; }
      "deps" "a";
    twoCycles = mergeCycles merge
      { a = { name = "a"; deps = [ "b" "c" ]; };
        b = { name = "b"; deps = [ "a" "d" ]; };
        c = { name = "c"; deps = [ "d" ]; };
        d = { name = "d"; deps = [ "c" ]; }; }
      "deps" "a";
    cycleBelow = mergeCycles merge
      { a = { name = "a"; deps = [ "b" ]; };
        b = { name = "b"; deps = [ "c" ]; };
        c = { name = "c"; deps = [ "b" ]; }; }
      "deps" "a";
    cycleInCycle = mergeCycles merge
      { a = { name = "a"; deps = [ "b" ]; };
        b = { name = "b"; deps = [ "c" ]; };
        c = { name = "c"; deps = [ "d" ]; };
        d = { name = "d"; deps = [ "c" "a" ]; };
      }
      "deps" "a";
  }
) {}
